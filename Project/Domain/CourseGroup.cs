﻿namespace Project.Domain;

public partial class CourseGroup
{
    public int Id { get; set; }

    public string GroupName { get; set; } = null!;

    public int CourseId { get; set; }

    public DateOnly? StartEducation { get; set; }

    public DateOnly? EndEducation { get; set; }

    public virtual Course Course { get; set; } = null!;

    public virtual ICollection<StudentInGroup> StudentInGroups { get; set; } = new List<StudentInGroup>();
}
