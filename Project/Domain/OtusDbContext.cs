﻿using Microsoft.EntityFrameworkCore;

namespace Project.Domain;

public partial class OtusDbContext : DbContext
{
    public OtusDbContext()
    {
    }

    public OtusDbContext(DbContextOptions<OtusDbContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Course> Courses { get; set; }

    public virtual DbSet<CourseGroup> CourseGroups { get; set; }

    public virtual DbSet<Student> Students { get; set; }

    public virtual DbSet<StudentInGroup> StudentInGroups { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=OtusDB;Username=test_user;Password=123456789");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Course>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("pk_course_id");

            entity.ToTable("course", "otusdb");

            entity.HasIndex(e => e.CourseName, "idx_course").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CourseName)
                .HasMaxLength(100)
                .HasColumnName("course_name");
            entity.Property(e => e.Description)
                .HasMaxLength(250)
                .HasColumnName("description");
            entity.Property(e => e.Duration).HasColumnName("duration");
        });

        modelBuilder.Entity<CourseGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("pk_course_group_id");

            entity.ToTable("course_group", "otusdb");

            entity.HasIndex(e => e.GroupName, "idx_course_group").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.CourseId).HasColumnName("course_id");
            entity.Property(e => e.EndEducation).HasColumnName("end_education");
            entity.Property(e => e.GroupName)
                .HasMaxLength(20)
                .HasColumnName("group_name");
            entity.Property(e => e.StartEducation).HasColumnName("start_education");

            entity.HasOne(d => d.Course).WithMany(p => p.CourseGroups)
                .HasForeignKey(d => d.CourseId)
                .HasConstraintName("fk_course_group_course");
        });

        modelBuilder.Entity<Student>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("pk_student_id");

            entity.ToTable("student", "otusdb");

            entity.HasIndex(e => e.Email, "idx_student").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Email)
                .HasMaxLength(100)
                .HasColumnName("email");
            entity.Property(e => e.Middlename)
                .HasMaxLength(100)
                .HasColumnName("middlename");
            entity.Property(e => e.Name)
                .HasMaxLength(100)
                .HasColumnName("name");
            entity.Property(e => e.Surname)
                .HasMaxLength(100)
                .HasColumnName("surname");
        });

        modelBuilder.Entity<StudentInGroup>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("pk_student_in_group_id");

            entity.ToTable("student_in_group", "otusdb");

            entity.HasIndex(e => new { e.GroupId, e.StudentId }, "idx_student_in_group").IsUnique();

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.GroupId).HasColumnName("group_id");
            entity.Property(e => e.StudentId).HasColumnName("student_id");

            entity.HasOne(d => d.Group).WithMany(p => p.StudentInGroups)
                .HasForeignKey(d => d.GroupId)
                .HasConstraintName("fk_student_in_group_course_group");

            entity.HasOne(d => d.Student).WithMany(p => p.StudentInGroups)
                .HasForeignKey(d => d.StudentId)
                .HasConstraintName("fk_student_in_group_student");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
