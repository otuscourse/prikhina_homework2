﻿namespace Project.Domain;

public partial class StudentInGroup
{
    public int Id { get; set; }

    public int GroupId { get; set; }

    public int StudentId { get; set; }

    public virtual CourseGroup Group { get; set; } = null!;

    public virtual Student Student { get; set; } = null!;
}
