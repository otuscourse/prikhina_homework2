﻿namespace Project.Domain;

public partial class Course
{
    public int Id { get; set; }

    public string CourseName { get; set; } = null!;

    public string Description { get; set; } = null!;

    public int Duration { get; set; }

    public virtual ICollection<CourseGroup> CourseGroups { get; set; } = new List<CourseGroup>();
}
