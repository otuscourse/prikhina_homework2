﻿using AutoMapper;

namespace Project.Models.StudentInGroup.Mapping
{
    public class StudentInGroupMapping
    {       
        public static Mapper InitializeAutomapperOut()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.StudentInGroup, StudentInGroupViewModel>()
                    .ForMember(d => d.StudentFIO, p => p.MapFrom(s =>
                    String.Concat(s.Student.Surname, " ", s.Student.Name, " ", s.Student.Middlename).Trim()))
                    .ForMember(d => d.GroupName, p => p.MapFrom(s => s.Group.GroupName));
            });
            var mapper = new Mapper(config);
            return mapper;
        }
        public static Mapper InitializeAutomapperIn()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StudentInGroupAddViewModel, Domain.StudentInGroup>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
