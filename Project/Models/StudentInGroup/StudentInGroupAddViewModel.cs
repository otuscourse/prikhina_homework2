﻿namespace Project.Models.StudentInGroup
{
    public class StudentInGroupAddViewModel
    {
        public int GroupId { get; set; }

        public int StudentId { get; set; }

    }
}
