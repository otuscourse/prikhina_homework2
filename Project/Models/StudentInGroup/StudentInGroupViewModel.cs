﻿namespace Project.Models.StudentInGroup
{
    public class StudentInGroupViewModel
    {
        public int Id { get; set; }

        public string StudentFIO { get; set; }

        public string GroupName { get; set; }
    }
}
