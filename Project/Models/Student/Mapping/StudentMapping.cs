﻿using AutoMapper;

namespace Project.Models.Student.Mapping
{
    public class StudentMapping
    {       
        public static Mapper InitializeAutomapperOut()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Student, StudentViewModel>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
        public static Mapper InitializeAutomapperIn()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StudentAddViewModel, Domain.Student>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
