﻿namespace Project.Models.Student
{
    public class StudentAddViewModel
    {
        public string Surname { get; set; } = null!;

        public string Name { get; set; } = null!;

        public string? Middlename { get; set; }

        public string Email { get; set; } = null!;

    }
}
