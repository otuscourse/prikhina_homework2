﻿namespace Project.Models.Student
{
    public class StudentViewModel
    {
        public int Id { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }

        public string? Middlename { get; set; }

        public string Email { get; set; }
    }
}
