﻿namespace Project.Models.CourseGroup
{
    public class CourseGroupAddViewModel
    {
        public int Id { get; set; }

        public string GroupName { get; set; } = null!;

        public int CourseId { get; set; }

        public DateOnly? StartEducation { get; set; }

        public DateOnly? EndEducation { get; set; }

    }
}
