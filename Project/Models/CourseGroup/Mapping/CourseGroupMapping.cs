﻿using AutoMapper;

namespace Project.Models.CourseGroup.Mapping
{
    public class CourseGroupMapping 
    {
        public static Mapper InitializeAutomapperOut()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.CourseGroup, CourseGroupViewModel>()
                 .ForMember(d => d.CourseName, p => p.MapFrom(s => s.Course.CourseName));
            });
            var mapper = new Mapper(config);
            return mapper;
        }
        public static Mapper InitializeAutomapperIn()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CourseGroupAddViewModel, Domain.CourseGroup>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
