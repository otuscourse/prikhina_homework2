﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.CourseGroup
{
    public class CourseGroupViewModel
    {
        public int Id { get; set; }

        public string GroupName { get; set; }

        public string CourseName { get; set; }

        public DateOnly? StartEducation { get; set; }

        public DateOnly? EndEducation { get; set; }
    }
}
