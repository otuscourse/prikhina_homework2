﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project.Models.Course
{
    public class CourseAddViewModel
    {
        public string CourseName { get; set; } = null!;

        public string Description { get; set; } = null!;

        public int Duration { get; set; }
    }
}
