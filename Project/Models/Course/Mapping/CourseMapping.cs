﻿using AutoMapper;

namespace Project.Models.Course.Mapping
{
    public class CourseMapping 
    {
        public static Mapper InitializeAutomapperOut()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Domain.Course, CourseViewModel>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
        public static Mapper InitializeAutomapperIn()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<CourseAddViewModel, Domain.Course>();
            });
            var mapper = new Mapper(config);
            return mapper;
        }
    }
}
