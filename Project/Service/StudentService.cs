﻿using Project.Domain;
using Project.Models.Course.Mapping;
using Project.Models.Student;
using Project.Models.Student.Mapping;

namespace Project.Service
{
    public class StudentService
    {
        public static void ShowAllOperations()
        {
            Console.WriteLine("\nВведите:\n" +
                    "StudentInfo (or s_info) - вывод содержимого таблицы Student\n" +
                    "AddStudent (or add_s) - добавление данных в таблицу Student\n");
            
            Console.Write("Ваш ввод: ");
            switch (Console.ReadLine().ToLower())
            {
                case "studentinfo" or "s_info":
                    OutputAllInfo();
                    break;
                case "add" or "add_s":
                    AddStudent();
                    break;
                default:
                    break;
            }
        }

        protected static List<StudentViewModel> GetAllInfo()
        {
            var students = new List<Student>();
            using (var context = new OtusDbContext())
            {
                students = context.Students.ToList();
            }
            var mapper = StudentMapping.InitializeAutomapperOut();

            return mapper.Map<List<StudentViewModel>>(students);
        }

        public static void OutputAllInfo()
        {
            var list = GetAllInfo();
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                Console.WriteLine($"Запись №{i+1}");
                Console.WriteLine($"Id = {item.Id}");
                Console.WriteLine($"Surname = {item.Surname}");
                Console.WriteLine($"Name = {item.Name}");
                if (!String.IsNullOrEmpty(item.Middlename))
                    Console.WriteLine($"Middlename = {item.Middlename}");
                Console.WriteLine($"Email = {item.Email}");
                Console.WriteLine();
            }            
        }

        public static void AddStudent()
        {
            var addItem = new StudentAddViewModel();
            Console.WriteLine("Введите фамилию студента:");
            addItem.Surname = GeneralService.CheckInputString(100);
            Console.WriteLine("Введите имя студента:");
            addItem.Name = GeneralService.CheckInputString(100);
            Console.WriteLine("Введите отчество студента:");
            addItem.Middlename = GeneralService.CheckInputString(100);
            Console.WriteLine("Введите email студента:");
            addItem.Email = GeneralService.CheckInputString(100);
            AddStudent(addItem);
        }


        public static void AddStudent(StudentAddViewModel addItem)
        {
            var mapper = StudentMapping.InitializeAutomapperIn();
            var item = mapper.Map<Student>(addItem);
            try
            {
                using (var context = new OtusDbContext())
                {
                    context.Students.Add(item);
                    context.SaveChanges();
                }
                Console.WriteLine($"Новая запись (Id = {item.Id}) успешно добавлена\n");
            }
            catch
            {
                Console.WriteLine("Не удалось добавить новую запись");
            }
        }
    }
}
