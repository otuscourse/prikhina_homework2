﻿using Project.Domain;
using Project.Models.CourseGroup;
using Project.Models.CourseGroup.Mapping;

namespace Project.Service
{
    public class CourseGroupService
    {
        static List<CourseGroup> courseGroups_rep;
        static List<Course> courses_rep;
        

        public static void ShowAllOperations()
        {
            Console.WriteLine("\nВведите:\n" +
                    "CourseGroupInfo (or cg_info) - вывод содержимого таблицы CourseGroup\n" +
                    "AddCourseGroup (or add_cg) - добавление данных в таблицу CourseGroup\n");

            Console.Write("Ваш ввод: ");
            switch (Console.ReadLine().ToLower())
            {
                case "coursegroupinfo" or "cg_info":
                    OutputAllInfo();
                    break;
                case "addcoursegroup" or "add_cg":
                    AddCourseGroup();
                    break;
                default:
                    break;
            }
        }

        protected static void LoadRep()
        {
            using (var context = new OtusDbContext())
            {
                courseGroups_rep = context.CourseGroups.ToList();
                courses_rep = context.Courses.ToList();
            }
        }

        protected static List<CourseGroupViewModel> GetAllInfo()
        {
            LoadRep();
            var groups = courseGroups_rep;
            foreach (var item in groups)
                item.Course = courses_rep.FirstOrDefault(c => c.Id == item.CourseId);
            
            var mapper = CourseGroupMapping.InitializeAutomapperOut();
            return mapper.Map<List<CourseGroupViewModel>>(groups);
        }

        public static void OutputAllInfo()
        {
            var list = GetAllInfo();
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                Console.WriteLine($"Запись №{i + 1}");
                Console.WriteLine($"Id = {item.Id}");
                Console.WriteLine($"GroupName = {item.GroupName}");
                Console.WriteLine($"StartEducation = {item.StartEducation}");
                Console.WriteLine($"EndEducation = {item.EndEducation}");
                Console.WriteLine();
            }
        }
        public static void AddCourseGroup()
        {
            GetAllInfo();
            var addItem = new CourseGroupAddViewModel();
            Console.WriteLine("Введите название группы:");
            addItem.GroupName = GeneralService.CheckInputString(100);
            Console.WriteLine("Введите Id курса:");
            addItem.CourseId = CheckCourseId();
            Console.WriteLine("Дата начала занятий:");
            addItem.StartEducation = GeneralService.CheckInputDate(false);
            Console.WriteLine("Дата окончания занятий:");
            addItem.EndEducation = GeneralService.CheckInputDate(true);
            AddCourseGroup(addItem);
        }

        public static void AddCourseGroup(CourseGroupAddViewModel addItem)
        {
            var mapper = CourseGroupMapping.InitializeAutomapperIn();
            var item = mapper.Map<CourseGroup>(addItem);
            try
            {
                using (var context = new OtusDbContext())
                {
                    context.CourseGroups.Add(item);
                    context.SaveChanges();
                }
                Console.WriteLine($"Новая запись (Id = {item.Id}) успешно добавлена\n");
            }
            catch
            {
                Console.WriteLine("Не удалось добавить новую запись");
            }
        }

        private static int CheckCourseId()
        {
            var property = GeneralService.CheckInputNumber();
            var check = courses_rep.Any(cg => cg.Id == property);
            while (!check)
            {
                Console.WriteLine("Курса с указанным Id не существует. Пожалуйста, повторите ввод.");
                property = GeneralService.CheckInputNumber();
                check = courses_rep.Any(cg => cg.Id == property);
            }
            return property;
        }

    }
}
