﻿using System.Text.RegularExpressions;

namespace Project.Service
{
    public class GeneralService
    {
        public static string CheckInputString(int stringlength)
        {
            var property = Console.ReadLine();
            while (property.Length > stringlength)
            {
                Console.WriteLine($"Введенная строка превышает лимит в {stringlength} символов. Пожалуйста, повторите ввод");
                property = Console.ReadLine();
            }
            return property;
        }

        public static int CheckInputNumber()
        {
            var property = Console.ReadLine();
            var check = Int32.TryParse(property, out var number);
            while(!check)
            {
                Console.WriteLine($"Введенная строка не является целым числом. Пожалуйста, повторите ввод");
                property = Console.ReadLine();
                check = Int32.TryParse(property, out number);
            }
            return number;
        }

        public static DateOnly? CheckInputDate(bool returnNull)
        {
            var property = Console.ReadLine();
            var check = DateOnly.TryParse(property, out var date);
            while (!check && !String.IsNullOrEmpty(property))
            {
                if (returnNull)
                    break;
                Console.WriteLine($"Введенная строка не является датой. Пожалуйста, повторите ввод");
                property = Console.ReadLine();
                check = DateOnly.TryParse(property, out date);
            }
            if (string.IsNullOrEmpty(property))
                return null;
            return date;
        }


    }
}
