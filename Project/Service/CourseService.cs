﻿using Project.Domain;
using Project.Models.Course;
using Project.Models.Course.Mapping;

namespace Project.Service
{
    public class CourseService
    {
        public static void ShowAllOperations()
        {
            Console.WriteLine("\nВведите:\n" +
                    "CourseInfo (or c_info) - вывод содержимого таблицы Course\n" +
                    "AddCourse (or add_c) - добавление данных в таблицу Course\n");

            Console.Write("Ваш ввод: ");
            switch (Console.ReadLine().ToLower())
            {
                case "courseinfo" or "c_info":
                    OutputAllInfo();
                    break;
                case "addcourse" or "add_c":
                    AddCourse();
                    break;
                default:
                    break;
            }
        }

        protected static List<CourseViewModel> GetAllInfo()
        {
            var courses = new List<Course>();
            using (var context = new OtusDbContext())
            {
                courses = context.Courses.ToList();
            }
            var mapper = CourseMapping.InitializeAutomapperOut();

            return mapper.Map<List<CourseViewModel>>(courses);
        }

        public static void OutputAllInfo()
        {
            var list = GetAllInfo();
            Console.WriteLine("Таблица Course");
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                Console.WriteLine($"Запись №{i + 1}");
                Console.WriteLine($"Id = {item.Id}");
                Console.WriteLine($"CourseName = {item.CourseName}");
                Console.WriteLine($"Description = {item.Description}");
                Console.WriteLine($"Duration = {item.Duration}");
                Console.WriteLine();
            }
        }

        public static void AddCourse()
        {
            var addItem = new CourseAddViewModel();
            Console.WriteLine("Введите название курса:");
            addItem.CourseName = GeneralService.CheckInputString(100);
            Console.WriteLine("Введите описание курса:");
            addItem.Description = GeneralService.CheckInputString(250);
            Console.WriteLine("Введите длительность курса (в месяцах):");
            addItem.Duration = GeneralService.CheckInputNumber();
            AddCourse(addItem);
        }

        protected static void AddCourse(CourseAddViewModel addItem)
        {
            var mapper = CourseMapping.InitializeAutomapperIn();
            var item = mapper.Map<Course>(addItem);
            try
            {
                using (var context = new OtusDbContext())
                {
                    context.Courses.Add(item);
                    context.SaveChanges();
                }
                Console.WriteLine($"Новая запись (Id = {item.Id}) успешно добавлена\n");
            }
            catch
            {
                Console.WriteLine("Не удалось добавить новую запись");
            }
        }

    }
}
