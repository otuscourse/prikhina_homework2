﻿namespace Project.Service
{
    public class MainMenu
    {
        public static void MenuShow()
        {           
            bool getOn = true;
            while (getOn)
            {
                Console.WriteLine("\nВведите:\n" +
                    "All - вывод содержимого всех таблиц\n" +
                    "Course (or c) - список операций с таблицей Course\n" +
                    "CourseGroup (or cg) - список операций с таблицей CourseGroup\n" +
                    "Student (or s) - список операций с таблицей Student\n" +
                    "StudentInGroup (or sig) - список операций с таблицей StudentInGroup\n" +
                    "Finish (or f) - завершение работы\n"); 
                Console.Write("Ваш ввод: ");

                switch (Console.ReadLine().ToLower())
                {
                    case "all":
                        OutAllInfo();
                        break;
                    case "course" or "c":
                        CourseService.ShowAllOperations();
                        break;
                    case "coursegroup" or "cg":
                        CourseGroupService.ShowAllOperations();
                        break;
                    case "student" or "s":
                        StudentService.ShowAllOperations();
                        break;
                    case "studentingroup" or "sig":
                        StudentInGroupService.ShowAllOperations();
                        break;
                    case "finish" or "f":
                        getOn = false;
                        break;
                    default:
                        break;
                }
            }
            Console.WriteLine("Завершение работы");
        }
        
        protected static void OutAllInfo()
        {
            Console.WriteLine();
            CourseService.OutputAllInfo();
            Console.WriteLine();
            Console.WriteLine("Таблица CourseGroup");
            CourseGroupService.OutputAllInfo();
            Console.WriteLine();
            Console.WriteLine("Таблица Student");
            StudentService.OutputAllInfo();
            Console.WriteLine();
            Console.WriteLine("Таблица StudentInGroup");
            StudentInGroupService.OutputAllInfo();
        }
    }
}
