﻿using Project.Domain;
using Project.Models.StudentInGroup;
using Project.Models.StudentInGroup.Mapping;

namespace Project.Service
{
    public class StudentInGroupService
    {
        static List<StudentInGroup> studentInGroups_rep;
        static List<CourseGroup> courseGroups_rep;
        static List<Student> students_rep;

        public static void ShowAllOperations()
        {
            LoadRep();
            Console.WriteLine("\nВведите:\n" +
                    "StudentInGroupInfo (or sig_info) - вывод содержимого таблицы StudentInGroup\n" +
                    "AddStudentInGroup (or add_sig) - добавление данных в таблицу StudentInGroup\n");

            Console.Write("Ваш ввод: ");
            switch (Console.ReadLine().ToLower())
            {
                case "studentingroupinfo" or "sig_info":
                    OutputAllInfo();
                    break;
                case "addstudentingroup" or "add_sig":
                    AddStudentInGroup();
                    break;
                default:
                    break;
            }
        }

        protected static void LoadRep()
        {
            using (var context = new OtusDbContext())
            {
                studentInGroups_rep = context.StudentInGroups.ToList();
                students_rep = context.Students.ToList();
                courseGroups_rep = context.CourseGroups.ToList();

            }
        }

        protected static List<StudentInGroupViewModel> GetAllInfo()
        {
            LoadRep();
            var sig = studentInGroups_rep;
            foreach (var item in sig)
            {
                item.Student = students_rep.FirstOrDefault(s => s.Id == item.StudentId);
                item.Group = courseGroups_rep.FirstOrDefault(g => g.Id == item.GroupId);
            }

            var mapper = StudentInGroupMapping.InitializeAutomapperOut();
            return mapper.Map<List<StudentInGroupViewModel>>(sig);
        }

        public static void OutputAllInfo()
        {
            var list = GetAllInfo();
            for (int i = 0; i < list.Count; i++)
            {
                var item = list[i];
                Console.WriteLine($"Запись №{i + 1}");
                Console.WriteLine($"Id = {item.Id}");
                Console.WriteLine($"StudentFIO = {item.StudentFIO}");
                Console.WriteLine($"GroupName = {item.GroupName}");
                Console.WriteLine();
            }
        }

        public static void AddStudentInGroup()
        {
            GetAllInfo();
            var addItem = new StudentInGroupAddViewModel();
            Console.WriteLine("Введите Id студента:");
            addItem.StudentId = CheckStudentId();
            Console.WriteLine("Введите Id группы:");
            addItem.GroupId =  CheckGroupId();
            AddStudentInGroup(addItem);
        }

        public static void AddStudentInGroup(StudentInGroupAddViewModel addItem)
        {
            var mapper = StudentInGroupMapping.InitializeAutomapperIn();
            var item = mapper.Map<StudentInGroup>(addItem);
            try
            {
                using (var context = new OtusDbContext())
                {
                    context.StudentInGroups.Add(item);
                    context.SaveChanges();
                }
                Console.WriteLine($"Новая запись (Id = {item.Id}) успешно добавлена\n");
            }
            catch
            {
                Console.WriteLine("Не удалось добавить новую запись");
            }
        }

        private static int CheckGroupId()
        {
            var property = GeneralService.CheckInputNumber();
            var check = courseGroups_rep.Any(cg => cg.Id == property);
            while (!check)
            {
                Console.WriteLine("Группы с указанным Id не существует. Пожалуйста, повторите ввод."); 
                property = GeneralService.CheckInputNumber();
                check = courseGroups_rep.Any(cg => cg.Id == property);
            }
            return property;
        }

        private static int CheckStudentId()
        {
            int property = -1;
            var check = false;
            while (!check)
            {
                Console.WriteLine("Студента с указанным Id не существует. Пожалуйста, повторите ввод.");
                property = GeneralService.CheckInputNumber();
                check = students_rep.Any(s => s.Id == property);
            }
            return property;
        }
    }
}
