INSERT INTO otusdb.course (course_name, description, duration) 
VALUES 
('C# ASP.NET Core �����������','���� ��� C#-������������� � ������ �� 2-3 ���, ������� ����� ���������� � �������� web-����������, ����� fullstack-�������������.',5),
('Python Developer','������������� ��� ���, ��� ����� � ���� ������� �������������� ��������� Python-����������� �� �����������, ��������������� Middle ������.',10),
('iOS Developer. Basic','���� ��� ���, ��� ����� ������� ������������� iOS-������������ ��� ����� �������������� ����������� ���� ������ ����������.',4),
('Scala-�����������','���� ��������� �� Java-������������� � ������ ������ �� 1 ����.',5),
('Golang Developer. Professional','���� �������� backend-�������������, ������� ����� ������� � Golang � ��������� ���� ������� ����������.',5);

INSERT INTO otusdb.course_group(group_name, course_id, start_education)
VALUES
('C#-2023-04',1,'2023-04-27'),
('C#-2023-07',1,'2023-07-18'),
('C#-2023-10',1,'2023-10-31'),
('Python-2023-08',2,'2023-08-30'),
('Golang-2023-09',2,'2023-08-28');

INSERT INTO otusdb.student(surname, name, middlename, email)
VALUES
('���������','�������','��������','potapenko_ai@gmail.com'),
('������','���������','����������','ea_gurovaAyandex.ru'),
('�������','����','�����������','ug_horoshev@gmail.com'),
('����������','������','����������','petrischenko_ad@mail.ru'),
('�������','����','�������','uu_gromova@yandex.ru');

INSERT INTO otusdb.student_in_group(group_id, student_id)
VALUES
(1,1),
(1,2),
(2,3),
(3,4),
(4,1),
(4,5);