CREATE SCHEMA otusdb;

CREATE  TABLE otusdb.course ( 
	id                   serial  NOT NULL ,
	course_name          varchar(100)  NOT NULL ,
	description          varchar(250)  NOT NULL ,
	duration             integer  NOT NULL ,
	CONSTRAINT pk_course_id PRIMARY KEY ( id ),
	CONSTRAINT idx_course UNIQUE ( course_name ) 
 );

CREATE  TABLE otusdb.course_group ( 
	id                   serial  NOT NULL ,
	group_name           varchar(20)  NOT NULL ,
	course_id            integer  NOT NULL ,
	start_education      date   ,
	end_education        date   ,
	CONSTRAINT pk_course_group_id PRIMARY KEY ( id ),
	CONSTRAINT idx_course_group UNIQUE ( group_name ) 
 );

CREATE  TABLE otusdb.student ( 
	id                   serial  NOT NULL ,
	surname              varchar(100)  NOT NULL ,
	name                 varchar(100)  NOT NULL ,
	middlename           varchar(100)   ,
	email                varchar(100)  NOT NULL ,
	CONSTRAINT pk_student_id PRIMARY KEY ( id ),
	CONSTRAINT idx_student UNIQUE ( email ) 
 );

CREATE  TABLE otusdb.student_in_group ( 
	id                   serial  NOT NULL ,
	group_id             integer  NOT NULL ,
	student_id           integer  NOT NULL ,
	CONSTRAINT pk_student_in_group_id PRIMARY KEY ( id ),
	CONSTRAINT idx_student_in_group UNIQUE ( group_id, student_id ) 
 );

ALTER TABLE otusdb.course_group ADD CONSTRAINT fk_course_group_course FOREIGN KEY ( course_id ) REFERENCES otusdb.course( id ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE otusdb.student_in_group ADD CONSTRAINT fk_student_in_group_course_group FOREIGN KEY ( group_id ) REFERENCES otusdb.course_group( id ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE otusdb.student_in_group ADD CONSTRAINT fk_student_in_group_student FOREIGN KEY ( student_id ) REFERENCES otusdb.student( id ) ON DELETE CASCADE ON UPDATE CASCADE;

